class player:

    def __init__(self, name):
        self.name = name
        self.score = 0
        self.is_playing = 1
        self.found_artist = 0
        self.found_name = 0
        self.last_score = 0

    def reset(self):
        self.is_playing = 1
        self.found_artist = 0
        self.found_name = 0
        return self

    def _found_name(self):
        self.found_name = 1
        if self.found_artist == 1:
            self.is_playing = 0

    def _found_artist(self):
        self.found_artist = 1
        if self.found_name == 1:
            self.is_playing == 0

    def _found_both(self):
        self.is_playing = 0
        self.found_artist = 1
        self.found_name = 1

    def get_score(self):
        return self.score

    def _score(self, points):
        self.score += points
        self.last_score = points

class players:

    def __init__(self):
        self.players = []
        self.number = 0
        self.artist = 0
        self.name = 0

    def add_player(self, name):
        self.players.append(player(name))
        self.number += 1

    def remove_player(self, name):
        for player in self.players:
            if name == player.name:
                self.players.remove(player)
        self.number -= 1

    def who_is_playing(self):
        p = []
        for player in self._players:
            p.append(player.name)
        return p

    def increase_score(self, name, score):
        s = 0
        for player in self.players:
            if name == player.name:
                if score == 1 and player.found_name == 0:
                    s = self.number - self.name
                    self.name += 1
                    player._found_name()
                elif score == 2 and player.found_artist == 0:
                    s = self.number - self.artist
                    self.artist += 1
                    player._found_artist()
                elif (score == 3 or score == 4) and player.found_artist == 0\
                        and player.found_name == 0:
                    player._found_both()
                    s = (self.number - self.artist) + (self.number - self.name)
                    self.name += 1
                    self.artist += 1
                player._score(s)

    def is_name_player(self, name):
        for player in self.players:
            if name == player.name and player.is_playing == 1:
                return True
        return False

    def get_players_name(self):
        names = []
        for player in self.players:
            names.append(player.name)
        return names

    def get_last_score(self, name):
        for player in self.players:
            if name == player.name:
                return player.last_score

    def get_scores(self):
        string = ''
        for player in self.players:
            string += f'{player.name}: {player.score}\n'
        string = string[:-1]
        return string

    def reset(self):
        self.players = list(map(player.reset, self.players))
        self.artist = 0
        self.name = 0

    def reset_all(self):
        self.players.clear()
        self.artist = 0
        self.number = 0
        self.name = 0
