import spotipy
import discord
from discord.ext import commands, tasks
from discord import FFmpegPCMAudio
from threading import Thread
from tools.players import player, players
import spotipy
from spotipy import oauth2
from flask import session
from flask_session import Session
from .config import caches_folder
import os
import secrets
import uuid
import traceback
import sys

class server:

    def __init__(self, _id):
        self.server = _id
        self.status = "off"
        self.players = players()
        self.tracks = None
        self.voicechannel = None
        self.vc = None
        self.track_played = None
        self.song_queue = []
        self.auth = None
        self.token_info = None
        self.token = None
        self.sp = None
        self.uuid = None
        self.sp = None

    async def get_spotify_token(self, ctx):
        #Authentification ici
        self.uuid = str(uuid.uuid4())
        self.auth =\
                spotipy.oauth2.SpotifyOAuth(scope="user-read-private", cache_path=\
                caches_folder + self.uuid, show_dialog=True)
        self.token_info = self.auth.get_cached_token()
        auth_url = self.auth.get_authorize_url()
        embed = discord.Embed(
            title="Spotify Log in",
            url=auth_url,
            description="Please log into spotify. Then, paste the given command in this channel")
        await ctx.send(embed=embed)

    async def set_token(self, token, ctx):
        try:
            self.token_info = self.auth.get_access_token(token)
            self.token = self.token_info['access_token']
            self.sp = spotipy.Spotify(auth=self.token)
        except Exception:
            await ctx.send("The token you gave is not valid. Please try again")
            traceback.print_exception(*sys.exc_info())
        if self.is_logged():
            await ctx.send("Your spotify account is now linked to this server")

    def authenticate(self, ID, key):
        if ID != self.server or key != self.key:
            return False
        return True

    async def connect_to_voice(self, ctx):
        if self.vc is not None and self.vc.is_connected():
            return
        self.voice_channel = ctx.author.voice
        if self.voice_channel is not None:
            self.vc = await self.voice_channel.channel.connect()
        else:
            await ctx.send("Please Connect to a voice channel")
            return False
        return True

    def is_logged(self):
        if self.sp == None:
            return False
        return True

    def token_expired(self):
        if self.auth.is_token_expired(self.token_info):
            return True
        return False

    def refresh_token(self):
        self.token_info =\
                self.auth.refresh_access_token(self.token_info['refresh_token'])
        self.token = self.token_info['access_token']
        self.sp = spotipy.Spotify(auth=self.token)

    def end(self):
        self.vc.stop()
        self.song_queue.clear()
        self.track_played = None
        self.track = None
        self.status = "on"
        self.players.reset_all()

    async def _off(self):
        if self.vc is not None:
            self.vc.stop()
            await self.vc.disconnect()
        from .web import site, app
        if self.uuid is not None:
            try :
                os.remove(caches_folder + self.uuid)
            except Exception:
                 traceback.print_exception(*sys.exc_info())
        self.status = "off"
        self.players.reset_all()
        self.players = None
        self.tracks = None
        self.voicechannel = None
        self.vc = None
        self.track_played = None
        self.song_queue.clear()
        self.sp = None
        self.token = None
        self.token_info = None
        self.auth = None
        self.uuid = None
        self.sp = None
        self.key = None
