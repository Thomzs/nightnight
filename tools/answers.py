import string
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import CountVectorizer
import re
import sys
import numpy as np
import unicodedata
import Levenshtein

def clean_ponctuation(s):
    s = re.sub(r'\([^()]*\)', '', s)
    s = s.partition("feat")[0]
    s = s.partition(" - ")[0]
    s = re.sub("'", "", s)
    s = ''.join([word for word in s if word not in string.punctuation])
    return s

class compare_strings:

    def __init__(self, s1, s2):
        strings = []
        strings.append(s1.lower())
        strings.append(s2.lower())
        strings = list(map(clean_ponctuation, strings))
        strings = list(map(self.clean_accents, strings))
        self.lev = Levenshtein.distance(strings[0], strings[1])
        self.sim = self.compare_vectors(strings)

    def clean_accents(self, s):
        try:
            s = unicode(s, 'utf-8')
        except NameError:
            pass
        s = unicodedata.normalize('NFD', s).encode('ascii', 'ignore')\
           .decode("utf-8")
        return s

    def cosine_sim_vectors(self, vec1, vec2):
        vec1 = vec1.reshape(1, -1)
        vec2 = vec2.reshape(1, -1)
        return cosine_similarity(vec1, vec2)[0][0]

    def compare_vectors(self, strings):
        vectorizer = CountVectorizer().fit_transform(strings)
        vectors = vectorizer.toarray()
        csim = self.cosine_sim_vectors(vectors[0], vectors[1])
        return csim

    def similarity(self, threshold):
        return True if self.sim >= threshold and self.lev < 4 else False


class answer_process:

    def __init__(self, answer, name, artists):
        name = clean_ponctuation(name)
        for artist in artists:
            s = artist['name'] + ' ' + name
            if compare_strings(s, answer).similarity(0.66) == True:
                self._result = 3
                return
            s = name + ' ' + artist['name']
            if compare_strings(s, answer).similarity(0.66) == True:
                self._result = 4
                return
            if compare_strings(artist['name'], answer).similarity(0.66) == True:
                self._result = 2
                return
        if compare_strings(name, answer).similarity(0.66) == True:
            self._result = 1
            return
        self._result = 0

    def result(self):
        return self._result
