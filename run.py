import discord
from discord.ext import commands, tasks
import random
from discord import FFmpegPCMAudio
import asyncio
from tools.answers import answer_process
from tools.players import player, players
from tools.server import server
from tools.config import HELP, caches_folder
import spotipy
from spotipy import oauth2
import os
from flask import Flask, session, request, redirect
from flask_session import Session
import uuid
from tools.config import servers
from threading import Thread
import traceback
import sys

#conf = tk.config_from_environment()
#token_spotify = tk.request_client_token(*conf[:2])
client = commands.Bot(command_prefix = '.')
client.remove_command('help')
#spotify = tk.Spotify(token_spotify, asynchronous=True)
playlist_id = '7oBeEkujcRybm7dCAUAIhG'

async def sendMsg(msg, ctx):
    await ctx.send(msg)

async def sendEmb(embed, ctx):
    await ctx.send(embed=embed)

async def _wait(time):
    await asyncio.sleep(time)

def play_next(ctx):
    try:
        artists = []
        ID = ctx.guild.id

        if ID not in servers:
            return
            servers[ID].status = "on"
        if servers[ID].track_played is not None:
            for artist in servers[ID].track_played['artists']:
                artists.append(artist['name'])

            embed = discord.Embed(
                title= "Answer",
                description = "The answer was: {} from {}".format(servers[ID].track_played['name'], ' and '.join(artists)))
            embed.set_thumbnail(url=servers[ID].track_played['album']['images'][0]['url'])
            send_fut = asyncio.run_coroutine_threadsafe(sendEmb(embed, ctx),
                loop)
            send_fut.result()
            servers[ID].track_played = None
            embed = discord.Embed(
                title = "Scores:",
                description = servers[ID].players.get_scores())
            send_fut = asyncio.run_coroutine_threadsafe(sendEmb(embed, ctx), loop)
            send_fut.result()

        if ID not in servers:
            return
        if servers[ID].song_queue is not None and len(servers[ID].song_queue) >= 1:
            del servers[ID].song_queue[0]
            source = servers[ID].song_queue[0]
            msg = 'Next song is about to start'
            servers[ID].players.reset()
            send_fut = asyncio.run_coroutine_threadsafe(sendMsg(msg, ctx), loop)
            send_fut.result()
            send_fut = asyncio.run_coroutine_threadsafe(_wait(5), loop)
            send_fut.result()
            servers[ID].track_played = source
            if servers[ID].vc is not None:
                servers[ID].status = "in_game"
                servers[ID].vc.play(FFmpegPCMAudio(source=source['preview_url']),
                    after=lambda x: play_next(ctx))
        else:
            embed = discord.Embed(
                title = "Final Scores:",
                description = servers[ID].players.get_scores())
            send_fut = asyncio.run_coroutine_threadsafe(sendEmb(embed, ctx), loop)
            send_fut.result()
            servers[ID].end()
    except:
        traceback.print_exception(*sys.exc_info())

@client.event
async def on_ready():
    print("Bot is ready")

@client.event
async def on_guild_remove(guild):
    ID = guild.id

    if ID in servers:
        await servers[ID]._off()
        del servers[ID]

@client.event
async def on_message(message):
    ID = message.guild.id

    if ID not in servers:
        await client.process_commands(message)
        return
    author = message.author.name
    if servers[ID].status != "in_game" or author == "NightNight"\
            or not servers[ID].players.is_name_player(author):
                await client.process_commands(message)
                return
    if message.content.startswith(".") or message.content.startswith('/'):
        await client.process_commands(message)
        return

    if servers[ID].track_played == None:
        return
    await message.delete()

    ret = answer_process(message.content,
            servers[ID].track_played['name'],
            servers[ID].track_played['artists']).result()
    if ret == 0:
        await message.channel.send(f'Try again, {author}')
        return
    elif ret == 1:
        mess = 'name'
    elif ret == 2:
        mess = 'artist'
    elif ret == 3 or ret == 4:
        mess = 'both name and artist'
    servers[ID].players.increase_score(author, ret)
    await message.channel.send('{} just scored {} points by guessing {}'\
            .format(author, servers[ID].players.get_last_score(author), mess))


class audio:

    @client.command(aliases=['p', 'stop'])
    async def pause(ctx):
        ID = ctx.guild.id

        if ID not in servers:
            return
        if servers[ID].status != "in_game":
            return
        servers[ID].vc.pause()
        servers[ID].status = "pause"

    @client.command()
    async def resume(ctx):
        ID = ctx.guild.id

        if ID not in servers:
            return
        if servers[ID].status != "pause":
            return
        servers[ID].vc.resume()
        servers[ID].status = "in_game"

    async def get_playlist(p, ctx, ID):
        market = servers[ID].sp.me()["country"]
        playlist = servers[ID].sp.playlist_items(playlist_id=p, market=market)
        if playlist is None:
            await ctx.send("Spotify Error")
            return
        try:
            _tracks = playlist['items']
        except KeyError:
            await ctx.send("Please try again. If you get this message again,\
                    please try another playlist")
            return None
        total = playlist['total']

        while total > len(_tracks) and len(_tracks) <= 300:
            _len = len(_tracks)
            playlist = servers[ID].sp.playlist_items(p, offset=_len)
            if playlist is None:
                await ctx.send("Spotify Error")
                return
            _tracks += playlist['items']
        random.shuffle(_tracks)
        return _tracks


class game_settings:

    @client.command()
    async def help(ctx):
        await ctx.send(HELP)

    @client.command()
    async def run(ctx):
        ID = ctx.guild.id

        if ID not in servers:
            servers[ID] = server(ID)
        if servers[ID].status == "off":
            await ctx.send('Starting the game\nEnter \".join\"'\
                    + ' if you wish to play')
            await servers[ID].get_spotify_token(ctx)
            servers[ID].status = "on"

    @client.command()
    async def token(ctx, *, token):
        ID = ctx.guild.id

        if ID not in servers or servers[ID].is_logged() or servers[ID].status != "on":
            return
        await servers[ID].set_token(token, ctx)

    @client.command()
    async def join(ctx):
        ID = ctx.guild.id

        if ID not in servers:
            return
        if servers[ID].status == "off":
            return
        elif servers[ID].status == "on":
            if not servers[ID].players.is_name_player(ctx.author.name):
                await ctx.send(f'{ctx.author.name} is now playing')
                servers[ID].players.add_player(ctx.author.name)
            else:
                await ctx.send(f'{ctx.author.name} is already registered')
        else:
            await ctx.send('Game is up, wait until it finishes to join')

    @client.command()
    async def quit(ctx):
        ID = ctx.guild.id

        if ID not in servers:
            return
        if servers[ID].status == "off" and servers[ID].players \
                and servers[ID].players.is_name_player(ctx.author.name) == False:
                    return
        elif servers[ID].status not in ["in_game", "pause"]:
            await ctx.send(f'{ctx.author.name} left the game')
        servers[ID].players.remove_player(ctx.author.name)

    @client.command()
    async def whoisplaying(ctx):
        ID = ctx.guild.id

        if ID not in servers:
            return
        if servers[ID].status == "off":
            return
        if servers[ID].players is None or servers[ID].players.number == 0:
            await ctx.send('No players registered at the moment')
        else:
            names = servers[ID].players.get_players_name()
            await ctx.send('Registered players: {}'.format(', '.join(names)))

    @client.command()
    async def playlist(ctx, *, command):
        ID = ctx.guild.id

        if ID not in servers:
            return
        if servers[ID].status != "on":
            return
        if not servers[ID].is_logged():
            await ctx.send("Please loggin with spotify before setting playlist")
            return
        playlist = command.partition("playlist/")[2]
        if not playlist:
            await ctx.send("Please give a valid spotify url")
            return
        if servers[ID].token_expired():
            servers[ID].refresh_token()
        try:
            servers[ID].tracks = await audio.get_playlist(playlist, ctx, ID)
        except:
            await ctx.send("Please give a valid spotify url, or try again")
        if servers[ID].tracks is None:
            await ctx.send("Please give a valid spotify url")
        else:
            await ctx.send("Got the playlist")

    @client.command()
    async def start(ctx):
        ID = ctx.guild.id
        if ID not in servers:
            return
        if servers[ID].status != "on":
            return
        if not servers[ID].players or servers[ID].players.number == 0:
            await ctx.send("No players registered yet")
            return
        if not servers[ID].is_logged():
            await ctx.send("Please loggin with spotify before starting")
            return
        if await servers[ID].connect_to_voice(ctx) == False:
            return
        await ctx.send("Starting game")

        if servers[ID].token_expired():
            servers[ID].refresh_token()
        if servers[ID].tracks is None:
            try:
                servers[ID].tracks = await audio.get_playlist(playlist_id, ctx, ID)
            except:
                await ctx.send("Please try again")
                return
        if servers[ID].tracks is None or len(servers[ID].tracks) == 0:
            ctx.send("Please try again")
            return
        servers[ID].status = "in_game"
        for song in servers[ID].tracks:
            if not song or not song['track']['preview_url']:
                continue
            source = song['track']
            servers[ID].song_queue.append(source)
            if not servers[ID].vc.is_playing():
                servers[ID].track_played = source
                servers[ID].vc.play(FFmpegPCMAudio(source=source['preview_url']),
                        after=lambda x: play_next(ctx))

    @client.command()
    async def end(ctx):
        ID = ctx.guild.id

        if ID not in servers:
            return
        embed = discord.Embed(
                title = "Final Scores:",
                description = servers[ID].players.get_scores())
        await ctx.send(embed = embed)
        servers[ID].end()

    @client.command()
    async def off(ctx):
        ID = ctx.guild.id

        if ID not in servers:
            return
        if servers[ID].status == "off":
            return
        await servers[ID]._off()
        del servers[ID]

def main():
    global loop

    loop = asyncio.get_event_loop()
    asyncio.set_event_loop(loop)
    if not os.path.exists(caches_folder):
        os.makedirs(caches_folder)
#    thread = Thread(target=app.run, kwargs={'threaded':True, 'host':'0.0.0.0'})
#    thread.start()
    client.run(os.environ['DISCORD_TOKEN'])
#    thread.join()

if __name__ == '__main__':
    main()
