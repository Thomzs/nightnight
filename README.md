**
#README

##Add it to your server

link link link

##Usage

.run to awake the bot - You will need to click on the link to connect your spotify account to the bot
.off to make it sleep - It will disconnect your spotify account from the bot
.join to join a game before it has started
.quit to unjoin a game before it has started
.whoisplaying to see actually registered players
'.playlist playlist_link' to set a playlist. This is optional, we provide a basic playlist, but if you are not French, you will probably struggle with it :-)
.start to start a game
.end to end a game
.help to display this message
During a game, simply answer by typing in the chat as if you were sending a random message.
I strongly advice you to create a NighNight text channel and to authorize the bot to read messages only in that channel.
Enjoy !

##Please consider donating

I am financing servers alone, and maintenance takes time. Every donation is helping me a lot :-)

##You found an issue?

Please open a new issue on the project page, if it was not already raised**
